import _ from 'lodash';

const array = [
  'Иван Иванович Иванов',
  'Иван Иванович Иванов2',
  'Tinna Gunnlaugsdóttir',
  'Иван Иванович  Иванов',
  'Иван Иванович Иванов Ивановов',
  'Иван 2Иванович  Иванов',
  'Иван Иванов',
  'Иван Иванович Иванов ',
  'Иван ивановиЧ Иванов',
  'иван иванович иванов',
  'Иванов',
  'Иван  Иванов',
  'Иван 22222',
  '2Иванов',
  ' Иванов',
  ' Иванов ',
  '',
  'Иван\' Иванов',
  'Иванов_Иван_Иванович',
  'Иванов/Иван/Иванович',
  'иГоРь аЛексАндРовиЧ сУвороВ'
];

array.forEach((fullname) => {
  // провеярем, не пустая ли строка
  if (fullname.length) {

    // разбиваем строку и избавляемся от лишних пробелов
    let fio = _.compact(fullname.split(' '));
    console.log(fio);

    // проверяем, чтобы ФИО содержало не больше 3 значений
    if (fio.length <= 3) {

      // проверяем, не содержит ли фамилия цифры
      if (!/\d|[_/!@#$%^&*()+=|?]/.test(_.last(fio))) {

        // приводим фамилию к нормальному виду
        const lastname = _.capitalize(_.last(fio));

        // убираем фамилию из массива
        fio = _.dropRight(fio);

        // получаем инициалы
        let initials = '';
        if (fio.length) {
          let result = true;
          fio.forEach((value) => {
            if (!/\d|[_/!@#$%^&*()+=|?]/.test(value)) {
              initials += ` ${_.capitalize(value.charAt(0))}.`;
            }
            else {
              result = false;
            }
          });

          if (result) {
            console.log(lastname + initials);
          }
          else {
            console.log('Invalid fullname');
          }
        }
        else {
          console.log(lastname);
        }
      }
      else {
        console.log('Invalid fullname');
      }
    }
    else {
      console.log('Invalid fullname');
    }
  }
  else {
    console.log('Invalid fullname');
  }

  console.log('');
});
