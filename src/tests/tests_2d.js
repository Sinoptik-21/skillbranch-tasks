import _ from 'lodash';
import hsl from '../lib/hslToHex';

const array = [
  { color: 'aaafff' },
  { color: '000fff' },
  { color: 'ABCDEF' },
  { color: '1A2B3C' },
  { color: '000000' },
  { color: 'fff' },
  { color: 'abc' },
  { color: '000' },
  { color: '%20abcdef' },
  { color: '-123456' },
  { color: '00' },
  { color: '' },
  { color: ' ' },
  { color: 'bcdefg' },
  { color: 'abcdeff' },
  { color: '0bcdeff' },
  { colour: 'abcdef' },
  { colour: 'abcd' },
  { color: '#ababab' },
  { color: '#aba' },
  { color: '#232323' },
  { color: '#abcd' },
  { color: '##ababab' },
  { color: 'rgb(0, 255, 64)' },
  { color: 'rgb(255, 0, 0)' },
  { color: 'rgb(128, 128, 128)' },
  { color: '   rgb(  0, 255  , 64  )' },
  { color: 'rgb(128, 128, 257)' },
  { color: 'rgb(128, 128, 256)' },
  { color: 'rgb(128, 128)' },
  { color: '#rgb(128, 128, 128)' },
  { color: 'rgb(128, 128, 128, 128)' },
  { color: 'hsl(195,%20100%,%2050%)' },
  { color: 'hsl(0,%2065%,%2050%)' },
  { color: 'hsl(0,%200%,%200%)' },
  { color: 'hsl(0,%20101%,%200%)' },
  { color: 'hsl(0,%20-100%,%200%)' },
  { color: 'hsl(0,%20100,%200%)' },
  { color: 'hsl(0, 0, 0)' },
  { color: '0' },
  { color: 'xyz(1, 1, 1)' },
  { color: 'rgb()' },
  { color: 'hsl()' },
  { color: 'hsl(359, 0%, 25%)' },
  { color: 'hsl(361, 0%, 25%)' },
  { color: 'hsl(red, green, blue)' },
];

array.forEach((urlColor) => {
  console.log(urlColor);
  if (urlColor.color === undefined || urlColor.color === '') return console.log('Invalid color\n');

  // удаляем лишние символы
  const url = urlColor.color.replace(/\s/g,'').replace(/%20/g, '').replace(/%25/g, '').toLocaleLowerCase();

  let color = '';

  // RGB
  if (/^rgb\(/i.test(url)) {
    const aRGB = url.match(/^rgb\((\d{1,3}?),(\d{1,3}?),(\d{1,3}?)\)$/i);
    if (
      aRGB === null ||
      aRGB[1] < 0 || aRGB[1] > 255 ||
      aRGB[2] < 0 || aRGB[2] > 255 ||
      aRGB[3] < 0 || aRGB[3] > 255
    ) return console.log('Invalid color\n');
    for (let i=1; i<=3; i++) {
      color += (+aRGB[i]).toString(16).replace(/^(.)$/,'0$1');
    }
  }
  // HSL
  else if (/^hsl\(/i.test(url)) {
    const aHSL = url.match(/^hsl\((\d{1,3}),(\d{1,3}[%]),(\d{1,3}[%])\)$/i);
    if (aHSL === null) return console.log('Invalid color\n');
    aHSL[2] = aHSL[2].replace('%', '');
    aHSL[3] = aHSL[3].replace('%', '');

    if (
      aHSL[1] < 0 || aHSL[1] > 360 ||
      aHSL[2] < 0 || aHSL[2] > 100 ||
      aHSL[3] < 0 || aHSL[3] > 100
    ) return console.log('Invalid color\n');

    color = hsl(aHSL[1], aHSL[2], aHSL[3]);
  }
  // HEX
  else {
    const reHEX = new RegExp('^(%23|#)?([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$');
    if (url.match(reHEX) === null) return console.log('Invalid color\n');
    color = (url.match(reHEX)[1] === '#')?url.match(reHEX)[2]:url.match(reHEX)[0];
  }

  if (color.length === 3) {
    let fullColor = '';
    for (let i=0; i<3; i++) {
      fullColor += color[i] + color[i];
    }
    color = fullColor;
  }
  console.log(`#${color}\n`);
});
