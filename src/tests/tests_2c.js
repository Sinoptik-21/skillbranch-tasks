import canonize from '../lib/canonize';

const array = [
  'https://telegram.me/skillbranch',
  'Https://Telegram.me/skillbranch',
  'Https://Telegram.mE/SkillBranch',
  'Https://Telegramff.mE/SkillBranch',
  'Https://Telegramdv.mE/SkillBranch',
  'https://Telegram.me/SkillBranch',
  '//telegram.me/skillbranch',
  'http://telegram.me/skillbranch',
  'telegram.me/skillbranch',
  'skillbranch',
  '@skillbranch',
  'https://vk.com/skillbranch',
  'http://vk.com/skillbranch',
  '//vk.com/skillbranch',
  'vk.com/skillbranch',
  'vk.com/skillbranch?w=wall-117903599_1076',
  'vk.com/skillbranch/profile',
  'vk.com/skill.branch',
  'https://vk.com/durov',
  'http://vk.com/durov',
  'http://www.vk.com/durov',
  '//vk.com/durov',
  'vk.com/durov',
  'durov',
  '@durov',
  'https://twitter.com/tjholowaychuk',
  'https://github.com/kriasoft/react-starter-kit',
  'vk.com/pavel.durov',
  'https://vk.com/pavel.durov/spam-url',
  'https://vk.com/pavel.durov/spam-url/vk.com/pavel.ne.durov',
  'http://www.vk.com/pavel.durov',
  'vk.com/pavel.durov',
  'https://twitter.com/pavel.durov',
  'http://xn--80adtgbbrh1bc.xn--p1ai/pavel.durov',
  'https://medium.com/@dan_abramov/mixins-are-dead-long-live-higher-order-components-94a0d2f9e750'
];

array.forEach((url) => {
  const username = canonize(url);
  console.log(username);
});
